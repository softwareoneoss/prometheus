package main

import (
    "fmt"
    "log"
    "net/http"
    "io/ioutil"
)

func handler(w http.ResponseWriter, r *http.Request) {
    fmt.Fprintf(w, "Prometheus için GO diliyle yazılmış örnek bir uygulamadır!")
}

func metricHandler(w http.ResponseWriter, r *http.Request) {
    files,_ := ioutil.ReadDir("/tmp")
    fmt.Fprintf(w, "#HELP tmp dizininde kaç dosya biriktiğini gösterir\n")
    fmt.Fprintf(w, "#TYPE custom_go_tmp_folder_len gauge or counter ?\n")
    fmt.Fprintf(w, "custom_go_tmp_folder_count %v", len(files))
}

func main() {
    http.HandleFunc("/", handler)
    http.HandleFunc("/metrics", metricHandler)
    log.Fatal(http.ListenAndServe(":4000", nil))
}