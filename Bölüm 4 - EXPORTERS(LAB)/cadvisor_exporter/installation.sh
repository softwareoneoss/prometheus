yum install docker -y
yum install docker-compose -y
groupadd docker
usermod -aG docker $(whoami)
systemctl enable docker