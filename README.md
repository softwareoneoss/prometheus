# PROMETHEUS WORKSHOP
SoftwareOne firmas� taraf�ndan 1 g�nl�k planlanan ve kurumlar�n monitoring ihtiya�lar�n� u�tan u�a kar��layan Prometheus �al��ma i�eri�idir.

## Giri�
* Teorik Anlat�m
* Architecture
* Prometheus Core
## Kurulum(LAB)
* Prometheus & Grafana Kurulum
* Kubernetes deployment
* HTTP(S) i�in SSL & Authentication
## Konfig�rasyon(LAB)
* Prometheus konfigurasyon
* Statik Targets
* Dinamik Targets & Service Discovery
## Exporters(LAB)
* Official Exporters
* Community Exporters
* CAdvisor Exporter(Docker Compose)
* Development: Custom Exporter(GO)
## Development(LAB)
* Client Library
* Development: Application Monitoring(Python Flask & MySQL)
## Query(LAB)
* PromQL logic 
* Samples
## Allert Manager(LAB)
* Kurulum
* Konfigurasyon

