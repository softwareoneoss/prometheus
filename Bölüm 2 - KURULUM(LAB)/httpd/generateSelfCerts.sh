yum install mod_ssl -y
mkdir /prometheus/certs
openssl req -new -x509 -nodes -sha1 -days 3650 -newkey rsa:2048 -subj "/C=TR/ST=Turkey/L=Istanbul/O=SoftwareOne/OU=Org/CN=*.swo.com/emailAddress=emre.yardimci@softwareone.com" -out /prometheus/certs/cert.pem -keyout /prometheus/certs/private.key
chown apache. -R /prometheus/certs/
chmod 777 -R /prometheus/